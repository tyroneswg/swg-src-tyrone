package script.csr;
//Notes: Remove resource container from the frog
//
import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Vector;
import java.lang.*;
import script.library.ai_lib;
import script.library.armor;
import script.library.beast_lib;
import script.library.buff;
import script.library.callable;
import script.library.chat;
import script.library.consumable;
import script.library.craftinglib;
import script.library.create;
import script.library.expertise;
import script.library.factions;
import script.library.gm;
import script.library.groundquests;
import script.library.healing;
import script.library.incubator;
import script.library.instance;
import script.library.jedi;
import script.library.loot;
import script.library.money;
import script.library.pet_lib;
import script.library.player_stomach;
import script.library.prose;
import script.library.resource;
import script.library.respec;
import script.library.skill;
import script.library.skill_template;
import script.library.space_crafting;
import script.library.space_flags;
import script.library.space_skill;
import script.library.space_transition;
import script.library.space_utils;
import script.library.static_item;
import script.library.stealth;
import script.library.sui;
import script.library.utils;
import script.library.weapons;
import script.library.performance;

public class csrtool extends script.base_script
{
    public csrtool()
    {
    }
    public static final String[] CSRTOOLPROMPT = 
    {
        " Format: /csrtool <command> (Case Sensitive)",
        " openlink -- Open a link for a player to assist with tickets, bugs and TOS violations.",
        " goto -- Opens a list of locations you can teleport to.",
        " getIdFromName -- Gets a obj_id from a player name.",
        " setCrafter -- Set 'made by' tag on item. ",
        " makeEventCloner -- Makes cloner for event. ",
        " persist -- persists object (your target)",
        " reimbursementchest -- spawns item in a chest for players to get after they lost it. 1 item per chest and place it in the house only.",
        " warpArea -- /csrtool warpArea [planet] [x] [y] [z] [range 0-300m]",
    };
    public static final String CSRTOOL_TITLE = "CSR Tool Commands:";
    public static final int GOD_LEVEL = 5; //replace to match us_admin.iff actually just make it a config jfc tyrone.
    public static final String STF = "city/city";
    public int OnAttach(obj_id self) throws InterruptedException
    {
        if (isGod(self))
        {
            if (getGodLevel(self) < GOD_LEVEL)
            {
                detachScript(self, "csr.csrtool");
                sendSystemMessage(self, "You do not have the appropriate access level to use this script.", null);
            }
        }
        return SCRIPT_CONTINUE;
    }
    public int cmdCsrTool(obj_id self, obj_id target, String params, float defaultTime) throws InterruptedException
    {
        if (isGod(self))
        {
            StringTokenizer st = new java.util.StringTokenizer(params);
            int tokens = st.countTokens();
            String command = "";
            if (st.hasMoreTokens())
            {
                command = st.nextToken();
            }
            if (command.equals(""))
            {
                sendSystemMessageTestingOnly(self, "CSR Tool: Use ? for help.");
                return SCRIPT_CONTINUE;
            }
            else if (command.equals("?"))
            {
                String allHelpData = "";
                Arrays.sort(CSRTOOLPROMPT);
                for (int i = 0; i < CSRTOOLPROMPT.length; i++)
                {
                    allHelpData = allHelpData + CSRTOOLPROMPT[i] + "\r\n\t";
                }
                saveTextOnClient(self, "csrCommandUsage.txt", allHelpData);
                sui.msgbox(self, self, allHelpData, sui.OK_ONLY, CSRTOOL_TITLE, "noHandler");
                return SCRIPT_CONTINUE;
            }
            else if ((command.equals("openlink")))
            {
                if (st.hasMoreTokens())
                {
                    String webserver = "swghope"; //replace with base name of website
                    String link = st.nextToken();
                    String buffer = "A CSR opened a webpage to help with your problem.";
                    String title = "Notice:";
                    if (!link.contains(webserver))
                    {
                        sendSystemMessageTestingOnly("self", "[openlink]: Link was not sent. link must remain on the" + webserver + " website and subdomains");
                        return SCRIPT_CONTINUE;
                    }
                    launchClientWebBrowser(target, link);
                    sui.msgbox(self, target, buffer, title);
                    sendSystemMessageTestingOnly(self, "attempted to open link on target.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("forceopenlink")))
            {
                if (st.hasMoreTokens())
                {
                    String link = st.nextToken();
                    launchClientWebBrowser(target, link);
                    return SCRIPT_CONTINUE;
                }
            }
            else if (command.equals("goto"))
            {
                sendSystemMessageTestingOnly(self, "Options: moseisley");
                if (st.hasMoreTokens())
                {
                    String location = st.nextToken();
                    if (location.equals("moseisley"))
                    {
                        String area = "tatooine";
                        float x = 3517.0f;
                        float z = 5.0f;
                        float y = -4791.0f;
                        warpPlayer(self, area, x, z, y, null, 0.0f, 0.0f, 0.0f);
                        return SCRIPT_CONTINUE;
                    }
                }
            }
            else if ((command.equals("getIdFromName")))
            {
                if (st.countTokens() == 1)
                {
                    String firstName = st.nextToken();
                    String buffer = "First Name is: " + firstName + " ID: " + (getPlayerIdFromFirstName(firstName)) + "";
                    String title = "CSR Tool";
                    sui.msgbox(self, self, buffer, title);
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("setCrafter")))
            {
                if (st.countTokens() == 1)
                {
                    obj_id crafter = utils.stringToObjId(st.nextToken());
                    if (st.countTokens() != 1)
                    {
                        sendSystemMessageTestingOnly(self, "Usage: /csrtool setCrafter OID");
                        return SCRIPT_CONTINUE;
                    }
                    else
                    {
                        setCrafter(target, crafter);
                    }
                }
            }
            else if (((command)).equals("setcount"))
            {
                if (st.hasMoreTokens())
                {
                    obj_id oid = getIntendedTarget(self);
                    int count = utils.stringToInt(st.nextToken());
                    setCount(oid, count);
                    sendSystemMessageTestingOnly(self, "Object: " + target + " has added " + count + " to it's item count.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("makeEventCloner")))
            {
                location cloneLoc = getWorldLocation(self);
                location cloneRespawn = getLocation(self);
                obj_id planet = getPlanetByName(cloneLoc.area);
                if (isIdValid(planet) || (isGod(self)))
                {
                    dictionary params1 = new dictionary();
                    params1.put("id", self);
                    params1.put("name", "[Event Cloner] - " + getName(self));
                    params1.put("buildout", "");
                    params1.put("areaId", self);
                    params1.put("loc", cloneLoc);
                    params1.put("respawn", cloneRespawn);
                    params1.put("type", 0);
                    messageTo(planet, "registerCloningFacility", params1, 1.0f, false);
                    String buffer = "Cloning Facility made. ID: " + planet;
                    String title = "CSR Tool";
                    sui.msgbox(self, target, buffer, title);
                }
                else
                {
                    sendSystemMessageTestingOnly(self, "Creation of Cloning Facility failed.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("persist")))
            {
                if (target != null && target != obj_id.NULL_ID)
                {
                    persistObject(target);
                    sendSystemMessageTestingOnly(self, "Persisted: " + target + "!");
                    return SCRIPT_CONTINUE;
                }
                return SCRIPT_CONTINUE;
            }
            else if ((command.equals("setCheater")))
            {
                //if (target != null && target != obj_id.NULL_ID)
                //{
                    setObjVar(target, "csr.cheater", 1);
                    sendSystemMessageTestingOnly(self, "Target added to cheater list. Use getCheaters to generate list.");
                    return SCRIPT_CONTINUE;
                //}
                //return SCRIPT_CONTINUE;
            }
            else if ((command.equals("getCheaters")))
            {
                if (st.hasMoreTokens())
                {
                    String range = st.nextToken();
                    String cheaterObjVar = "csr.cheater";
                    int range1 = utils.stringToInt(range);
                    obj_id[] objObjects = getCreaturesInRange(self, range1);
                    for (int intI = 0; intI < objObjects.length; intI++)
                    {
                        if (isIdValid(objObjects[intI]) && isPlayer(objObjects[intI]) && hasObjVar(objObjects[intI], cheaterObjVar))
                        {
                            String cheaters = "";
                            cheaters = cheaters + objObjects[intI];
                            int time = getGameTime();
                            saveTextOnClient(self, "cheaters_within_"+ range1 +"m-"+ time +".txt", cheaters);
                            sendSystemMessageTestingOnly(self, "Found cheaters and printed to text file.");
                        }
                        else
                        {
                            sendSystemMessageTestingOnly(self, "No players found. Aborting");
                            return SCRIPT_CONTINUE;
                        }
                    }
                }
                return SCRIPT_CONTINUE;
            }
            else if ((command.equals("helpFlag")))
            {
                if (st.hasMoreTokens())
                {
                    String toggle = st.nextToken();
                    if (toggle.equals("on"))
                    {
                        setCondition(self, CONDITION_INTERESTING);
                        sendSystemMessageTestingOnly(self, "Flag visible.");
                    }
                    else if (toggle.equals("off"))
                    {
                        clearCondition(self, CONDITION_INTERESTING);
                        sendSystemMessageTestingOnly(self, "Flag invisible.");
                    }
                    else
                    {
                        return SCRIPT_CONTINUE;
                    }
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("reimbursementchest")))
            {
                if (st.hasMoreTokens())
                {
                    
                    location csrLoc = getLocation(self);
                    obj_id treasureChest = createObject("object/tangible/container/drum/treasure_drum.iff", csrLoc);
                    String item = st.nextToken();
                    obj_id pInv = utils.getInventoryContainer(treasureChest);
                    if (item.endsWith(".iff"))
                    {
                        createObject(item, pInv, "");
                        sendSystemMessageTestingOnly(self, "Reimbursement Chest - Item Put inside: " + item);
                    }
                    else if ((item.contains("character_builder_terminal")))
                    {
                        sendSystemMessageTestingOnly(self, "Failed! - Illegal Action");
                    }
                    else if ((item.contains("god")))
                    {
                        sendSystemMessageTestingOnly(self, "Failed! - Illegal Action");
                    }
                    else
                    {
                        static_item.createNewItemFunction(item, pInv);
                        sendSystemMessageTestingOnly(self, "Reimbursement Chest - Item Put inside: " + item);
                    }
                    sendSystemMessageTestingOnly(self, "Done");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("createlootchest")))
            {
                if (st.hasMoreTokens())
                {
                    String lootTable = st.nextToken();
                    String lootNumber = st.nextToken();
                    int lootCount = utils.stringToInt(lootNumber);
                    location treasureLoc = getLocation(self);
                    obj_id treasureChest = createObject("object/tangible/container/drum/treasure_drum.iff", treasureLoc);
                    loot.makeLootInContainer(treasureChest, lootTable, lootCount, 0);
                    sendSystemMessageTestingOnly(self, "A loot chest was made with 20 items from the loot table: " + lootTable);
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("playcef"))
            {
                if (st.hasMoreTokens())
                {
                    String cef = st.nextToken();
                    playClientEffectObj(self, "clienteffect/" + cef + ".cef", self, "root");
                    sendSystemMessageTestingOnly(self, "Client Effect:" + cef + " played.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("attach"))
            {
                if (st.hasMoreTokens())
                {
                    String ascript = st.nextToken();
                    attachScript(self, ascript);
                    sendSystemMessageTestingOnly(self, "You have attached script:" + ascript + " to self. use /qatool detach to remove it.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("detach"))
            {
                if (st.hasMoreTokens())
                {
                    String dscript = st.nextToken();
                    detachScript(self, dscript);
                    sendSystemMessageTestingOnly(self, "You have detached script:" + dscript + " to self. use /qatool attach to add it again.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("playsound"))
            {
                if (st.hasMoreTokens())
                {
                    String sound = st.nextToken();
                    play2dNonLoopingSound(self, "sound/" + sound + ".snd");
                    sendSystemMessageTestingOnly(self, "Sound: " + sound + " is now playing.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("persist"))
            {
                if (target != null && target != obj_id.NULL_ID)
                {
                    persistObject(target);
                    sendSystemMessageTestingOnly(self, "Persisted: " + target + "!");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("sws"))
            {
                if (st.hasMoreTokens())
                {
                    String objectName1 = st.nextToken();
                    String scriptname = st.nextToken();
                    location spawnLocation1 = getLocation(self);
                    obj_id baseObj = create.object(objectName1, spawnLocation1, -1);
                    attachScript(baseObj, scriptname);
                    setYaw(baseObj, getYaw(self));
                    sendSystemMessageTestingOnly(self, "Spawned object and attached the script.");
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((toLower(command)).equals("generatelootfromtable"))
            {
                if (st.hasMoreTokens())
                {
                    String lootTable = st.nextToken();
                    obj_id inventoryContainer = getObjectInSlot(self, "inventory");
                    loot.makeLootInContainer(inventoryContainer, lootTable, 1, 0);
                    sendSystemMessageTestingOnly(self, "A loot itm was generated into your inventory from the table: " + lootTable);
                    return SCRIPT_CONTINUE;
                }
            }
            else if ((command.equals("warpArea")))
            {
                if (st.hasMoreTokens())
                {
                    String area = st.nextToken();
                    String x = st.nextToken();
                    String y = st.nextToken();
                    String z = st.nextToken();
                    String range = st.nextToken();
                    float x1 = utils.stringToFloat(x);
                    float y1 = utils.stringToFloat(y);
                    float z1 = utils.stringToFloat(z);
                    int range1 = utils.stringToInt(range);
                    obj_id[] objObjects = getCreaturesInRange(self, range1);
                    sendSystemMessageTestingOnly(self, "Selected Players");
                    for (int intI = 0; intI < objObjects.length; intI++)
                    {
                        if (isIdValid(objObjects[intI]) && isPlayer(objObjects[intI]))
                        {
                            warpPlayer(objObjects[intI], area, x1, y1, z1, null, 0.0f, 0.0f, 0.0f);
                            sendSystemMessageTestingOnly(self, "Warped players to: " + x +", "+ y + ", " + z + ", " + area + ".");
                        }
                        else
                        {
                            sendSystemMessageTestingOnly(self, "No players found. Aborting");
                            return SCRIPT_CONTINUE;
                        }
                    }
                }
            }
            else if ((command.equals("kneelandfreeze")))
            {
                sendSystemMessageTestingOnly(self, "Warning: your a pleb");
                return SCRIPT_CONTINUE;
            }
        }
        return SCRIPT_CONTINUE;
    }
}