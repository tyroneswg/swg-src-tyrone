package script.tcg;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.city;
import script.library.faction_perk;
import script.library.player_structure;
import script.library.prose;
import script.library.session;
import script.library.static_item;
import script.library.sui;
import script.library.turnstile;
import script.library.utils;

public class item_terminal extends script.base_script
{
    public item_terminal()
    {
    }

    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
    {
        
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
        return SCRIPT_CONTINUE;
    }
}
