// ======================================================================

include library.groundquests;

// ----------------------------------------------------------------------

inherits quest.task.ground.base_task;

// ----------------------------------------------------------------------

//-- data table columns
const String dataTableColumnActionName = "ACTION_NAME";

//-- task info
const String taskType = "perform_action_on_npc";

// ----------------------------------------------------------------------

trigger OnTaskActivated(int questCrc, int taskId)
{
	groundquests.questOutputDebugInfo(self, questCrc, taskId, taskType, "OnTaskActivated", taskType + " task activated.");
	groundquests.setBaseObjVar(self, taskType, questGetQuestName(questCrc), taskId);
	return super.OnTaskActivated(self, questCrc, taskId);
}

// ----------------------------------------------------------------------

trigger OnTaskCompleted(int questCrc, int taskId)
{
	cleanup(self, questCrc, taskId);
	groundquests.questOutputDebugInfo(self, questCrc, taskId, taskType, "OnTaskCompleted", taskType + " task completed.");
	return super.OnTaskCompleted(self, questCrc, taskId);
}

// ----------------------------------------------------------------------

trigger OnTaskFailed(int questCrc, int taskId)
{
	cleanup(self, questCrc, taskId);
	groundquests.questOutputDebugInfo(self, questCrc, taskId, taskType, "OnTaskFailed", taskType + " task failed.");
	return super.OnTaskFailed(self, questCrc, taskId);
}

// ----------------------------------------------------------------------

trigger OnTaskCleared(int questCrc, int taskId)
{
	cleanup(self, questCrc, taskId);
	groundquests.questOutputDebugInfo(self, questCrc, taskId, taskType, "OnTaskCleared", taskType + " task cleared.");
	return super.OnTaskCleared(self, questCrc, taskId);
}

// ----------------------------------------------------------------------

trigger OnDetach()
{
	removeObjVar(self, groundquests.getTaskTypeObjVar(self, taskType));
	return SCRIPT_CONTINUE;
}

// ----------------------------------------------------------------------

messageHandler questActionCompleted()
{
	int questCrc = params.getInt(groundquests.QUEST_CRC);
	int taskId   = params.getInt(groundquests.TASK_ID);

	groundquests.questOutputDebugInfo(self, questCrc, taskId, taskType, "questActionCompleted", taskType + " quest actionc complete received.");

	if (questIsTaskActive(questCrc, taskId, self))
	{
		groundquests.questOutputDebugLog(taskType, "questActionCompleted", "Action completed successfully. Quest complete.");
		questCompleteTask(questCrc, taskId, self);
	}

	return SCRIPT_CONTINUE;
}

// ----------------------------------------------------------------------

void cleanup(obj_id player, int questCrc, int taskId)
{
	groundquests.clearBaseObjVar(player, taskType, questGetQuestName(questCrc), taskId);
}

// ======================================================================
