package script.city.imperial_crackdown;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.ai.ai_combat;
import script.library.ai_lib;

public class imperial_backup extends script.base_script
{
    public imperial_backup()
    {
    }
    public int OnAttach(obj_id self) throws InterruptedException
    {
        messageTo(self, "goingToFight", null, 1, false);
        int x = rand(300, 600);
        messageTo(self, "cleanUp", null, x, false);
        return SCRIPT_CONTINUE;
    }
    public int OnArrivedAtLocation(obj_id self, String name) throws InterruptedException
    {
        if (name.equals("fight"))
        {
            obj_id attacker = getObjIdObjVar(self, "whoToFight");
            startCombat(self, attacker);
        }
        if (name.equals("done"))
        {
            messageTo(self, "leaveCantina", null, 7, false);
        }
        return SCRIPT_CONTINUE;
    }
    public int goingToFight(obj_id self, dictionary params) throws InterruptedException
    {
        location goFight = getLocationObjVar(self, "whereToFight");
        goFight.x = goFight.x + (rand(-5, 5));
        ai_lib.aiPathTo(self, goFight);
        addLocationTarget("fight", goFight, 1);
        return SCRIPT_CONTINUE;
    }
    public int cleanUp(obj_id self, dictionary params) throws InterruptedException
    {
        obj_id top = getTopMostContainer(self);
        obj_id foyer = getCellId(top, "foyer1");
        location here = getLocation(self);
        String planet = here.area;
        location impLoc = new location(47.02f, .1f, -2.93f, planet, foyer);
        ai_lib.aiPathTo(self, impLoc);
        addLocationTarget("done", impLoc, 1);
        messageTo(self, "handleBadLeaving", null, 60, false);
        return SCRIPT_CONTINUE;
    }
    public int leaveCantina(obj_id self, dictionary params) throws InterruptedException
    {
        destroyObject(self);
        return SCRIPT_CONTINUE;
    }
    public int handleBadLeaving(obj_id self, dictionary params) throws InterruptedException
    {
        messageTo(self, "leaveCantina", null, 7, false);
        return SCRIPT_CONTINUE;
    }
}
