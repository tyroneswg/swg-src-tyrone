package script.systems.rls;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;
import script.library.sui;
import script.library.utils;
import script.library.loot;
import script.library.create;
import script.library.static_item;

public class rls extends script.base_script
{
    public rls()
    {
    }
	public static final String STF_FILE = "npe";
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
    {
        int mnu2 = mi.addRootMenu(menu_info_types.ITEM_USE, new string_id(STF_FILE, "crate_use"));
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
        obj_id inventory = utils.getInventoryContainer(player);
		obj_id chest = self;
		String loot_table = getStringObjVar(chest, "loot_table");
			
		int loot_count = rand(1, 3);
		sendDirtyObjectMenuNotification(chest);
        if (item == menu_info_types.ITEM_USE)
        {
			loot.makeLootInContainer(inventory, "tcg/" + loot_table, loot_count, 1);
			destroyObject(chest);

        }
        return SCRIPT_CONTINUE;
    }
}