package script.tyrone;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.ai_lib;
import script.library.buff;
import script.library.callable;
import script.library.chat;
import script.library.conversation;
import script.library.groundquests;
import script.library.holiday;
import script.library.utils;

public class efol_reattach extends script.base_script
{
    public efol_reattach()
    {
    }
    public int OnInitialize(obj_id self) throws InterruptedException
    {
        detachScript(self, "tyrone.test_chak");
		attachScript(self, "tyrone.test_chak");
        return SCRIPT_CONTINUE;
    }
    public int OnAttach(obj_id self) throws InterruptedException
    {
        detachScript(self, "tyrone.test_chak");
		attachScript(self, "tyrone.test_chak");
        return SCRIPT_CONTINUE;
    }

}
