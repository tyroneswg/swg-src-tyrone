package script.tyrone;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.trial;
import script.library.restuss_event;
import script.library.regions;
import script.library.factions;

public class pvp_region extends script.base_script
{
    public pvp_region()
    {
    }
	public static final String REGION_NAME = "Admin Zone";
    public int OnAttach(obj_id self) throws InterruptedException
    {
        //persistObject(self);
        createCircleRegion(getLocation(self), 128, REGION_NAME, regions.PVP_REGION_TYPE_NORMAL, regions.BUILD_FALSE, regions.MUNI_TRUE, regions.GEO_CITY, 0, 0, regions.SPAWN_FALSE, regions.MISSION_NONE, false, true);
        obj_id[] players = getPlayerCreaturesInRange(getLocation(self), 130.0f);
        if (players == null || players.length == 0)
        {
            return SCRIPT_CONTINUE;
        }
        for (int i = 0; i < players.length; i++)
        {
            if(isGod(players[i]))
            {
				//setObjvar(self, "pvp.regions.entered", 1);
				sendSystemMessageTestingOnly(players[i], "Welcome to " + REGION_NAME);
                continue;
            }
			else if(!isGod(players[i]))
			{
				String scene = getCurrentSceneName();
				sendSystemMessageTestingOnly(players[i], "You must be in god mode to enter this area.");
				warpPlayer(players[i], scene, 0, 0, 0, null, 0, 0, 0);
				//setObjvar(self, "pvp.regions.entered", 1);
			}
			else
			{
				sendSystemMessageTestingOnly(players[i], "Not sure what to put here...");
			}
        }
        return SCRIPT_CONTINUE;
    }
    public int OnInitialize(obj_id self) throws InterruptedException
    {
        return SCRIPT_CONTINUE;
    }
    public int OnDestroy(obj_id self) throws InterruptedException
    {
		String scene = getCurrentSceneName();
        region pvpRegion = getRegion(scene, REGION_NAME);
        deleteRegion(pvpRegion);
        return SCRIPT_CONTINUE;
    }
}
