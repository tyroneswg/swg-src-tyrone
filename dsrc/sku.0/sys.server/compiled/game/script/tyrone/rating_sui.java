package script.tyrone;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.developer.file_access;
import script.library.utils;
import script.library.sui;
import java.io.InputStream;
import script.library.chat;
import java.util.Date;

public class rating_sui extends script.base_script
{
    public rating_sui()
    {
    }
    public int OnSpeaking(obj_id self, String text) throws InterruptedException
    {
        if (!isGod(self))
        {
            return SCRIPT_CONTINUE;
        }
        if (text.startsWith("testSurvey"))
        {
        String gcwHelpData = "1: Would you be likely to mention SWG:Legends to someone would used to play Star Wars Galaxies?";
        String uiTitle = "SWG:Legends Survey";
        int page = createSUIPage("/Script.questionnaire", self, self);
        setSUIProperty(page, "Prompt.lblPrompt", "LocalText", gcwHelpData);
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Submit");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleSurveyReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testRating"))
        {
        String gcwHelpData = "On a scale of 1-10. How is crafting going?";
        String uiTitle = "SWG:Legends Crafting Survey";
        int page = createSUIPage("/GroundHUD.ratingScreen", self, self);
        setSUIProperty(page, "Prompt.lblPrompt", "LocalText", gcwHelpData);
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Submit");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleSurveyReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testBar"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.barMenu", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testBug"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Debug.bug", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testIME"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/PDA.ime", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testService"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/PDA.service", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		//setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		//setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testBomb"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.disarmBomb", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		//setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		//setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testHair"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.Hairstyles", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		//setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		//setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testTabs"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.tabexample", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		//setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		//setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		if (text.startsWith("testOV"))
        {
        String gcwHelpData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.objectViewer", self, self);
        setSUIProperty(page, "comp.text", "Text", gcwHelpData);
		//setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		//setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
		}
		return SCRIPT_CONTINUE;
	}
	public int handleSurveyReward(obj_id self, dictionary params) throws InterruptedException
	{
		sendSystemMessageTestingOnly(self, "Survey Complete! Thank you for your time.");
		return SCRIPT_OVERRIDE;
	}
	public int handleBarReward(obj_id self, dictionary params) throws InterruptedException
	{
		sendSystemMessageTestingOnly(self, "Item Purchased.");
		return SCRIPT_OVERRIDE;
	}
}