package script.tyrone;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;

public class setstate extends script.base_script
{
    public setstate()
    {
    }
    public int OnAttach(obj_id self) throws InterruptedException
    {
        sendSystemMessageTestingOnly(self, "State script attached. Say `state_list` to list the basic states.");
        sendSystemMessageTestingOnly(self, "See script file for all");
        return SCRIPT_CONTINUE;
    }
    public int OnHearSpeech(obj_id self, obj_id speaker, String text) throws InterruptedException
    {
        if (speaker != self)
        {
            return SCRIPT_CONTINUE;
        }
        if (!isGod(self))
        {
            return SCRIPT_CONTINUE;
        }
        String[] argv = split(text, ' ');
        if (argv[0].equals("basic_state_list"))
        {
            sendSystemMessageTestingOnly(self, "state_cover, state_combat, state_acid_burned state_aiming state_berserk state_diseased state_swimming");
			sendSystemMessageTestingOnly(self, "See script for the rest.");
        }
        if (argv[0].equals("state_cover"))
        {
            setState(self, STATE_COVER, true);
        }
        if (argv[0].equals("state_combat"))
        {
            setState(self, STATE_COMBAT, true);
        }
        if (argv[0].equals("state_peace"))
        {
            setState(self, STATE_PEACE, true);
        }
        if (argv[0].equals("state_aiming"))
        {
            setState(self, STATE_AIMING, true);
        }
        if (argv[0].equals("state_meditate"))
        {
            setState(self, POSTURE_SNEAKING, true);
        }
        if (argv[0].equals("state_berserk"))
        {
            setState(self, STATE_BERSERK, true);
        }
        if (argv[0].equals("state_feigndeath"))
        {
            setState(self, STATE_FEIGN_DEATH, true);
        }
        if (argv[0].equals("state_ca_evasive"))
        {
            setState(self, STATE_CA_EVASIVE, true);
        }
        if (argv[0].equals("state_ca_normal"))
        {
            setState(self, STATE_CA_NORMAL, true);
        }
        if (argv[0].equals("sitting"))
        {
            setState(self, STATE_CA_AGGRESSIVE, true);
        }
        if (argv[0].equals("state_tumbling"))
        {
            setState(self, STATE_TUMBLING, true);
        }
        if (argv[0].equals("state_rallied"))
        {
            setState(self, STATE_RALLIED, true);
        }
        if (argv[0].equals("state_stunned"))
        {
            setState(self, STATE_STUNNED, true);
        }
        if (argv[0].equals("state_blinded"))
        {
            setState(self, STATE_BLINDED, true);
        }
        if (argv[0].equals("state_dizzy"))
        {
            setState(self, STATE_DIZZY, true);
        }
        if (argv[0].equals("state_intimidated"))
        {
            setState(self, STATE_INTIMIDATED, true);
        }
        if (argv[0].equals("state_immobilized"))
        {
            setState(self, STATE_IMMOBILIZED, true);
        }
        if (argv[0].equals("state_frozen"))
        {
            setState(self, STATE_FROZEN, true);
        }
        if (argv[0].equals("state_swimming"))
        {
            setState(self, STATE_SWIMMING, true);
        }
        if (argv[0].equals("state_sittingonchair"))
        {
            setState(self, STATE_SITTING_ON_CHAIR, true);
        }
        if (argv[0].equals("state_crafting"))
        {
            setState(self, STATE_CRAFTING, true);
        }
        if (argv[0].equals("state_glowing_jedi"))
        {
            setState(self, STATE_GLOWING_JEDI, true);
        }
        if (argv[0].equals("state_mask_scent"))
        {
            setState(self, STATE_MASK_SCENT, true);
        }
        if (argv[0].equals("state_poisoned"))
        {
            setState(self, STATE_POISONED, true);
        }
        if (argv[0].equals("state_bleeding"))
        {
            setState(self, STATE_BLEEDING, true);
        }
        if (argv[0].equals("state_diseased"))
        {
            setState(self, STATE_DISEASED, true);
        }
        if (argv[0].equals("state_on_fire"))
        {
            setState(self, STATE_ON_FIRE, true);
        }
        if (argv[0].equals("state_riding_mount"))
        {
            setState(self, STATE_RIDING_MOUNT, true);
        }
        if (argv[0].equals("state_mounted"))
        {
            setState(self, STATE_MOUNTED_CREATURE, true);
        }
        if (argv[0].equals("state_pship"))
        {
            setState(self, STATE_PILOTING_SHIP, true);
        }
        if (argv[0].equals("state_shipop"))
        {
            setState(self, STATE_SHIP_OPERATIONS, true);
        }
        if (argv[0].equals("state_sgunner"))
        {
            setState(self, STATE_SHIP_GUNNER, true);
        }
        if (argv[0].equals("state_ship_inside"))
        {
            setState(self, STATE_SHIP_INTERIOR, true);
        }
        if (argv[0].equals("state_pilot_pob"))
        {
            setState(self, STATE_PILOTING_POB_SHIP, true);
        }
        if (argv[0].equals("state_performdb"))
        {
            setState(self, STATE_PERFORM_DEATHBLOW, true);
        }
        if (argv[0].equals("state_disguise"))
        {
            setState(self, STATE_DISGUISE, true);
        }
        if (argv[0].equals("state_electric_burned"))
        {
            setState(self, STATE_ELECTRIC_BURNED, true);
        }
        if (argv[0].equals("state_cold_burned"))
        {
            setState(self, STATE_COLD_BURNED, true);
        }
        if (argv[0].equals("state_acid_burned"))
        {
            setState(self, STATE_ACID_BURNED, true);
        }
        if (argv[0].equals("state_energy_burned"))
        {
            setState(self, STATE_ENERGY_BURNED, true);
        }
        if (argv[0].equals("state_kinetic_burned"))
        {
            setState(self, STATE_KINETIC_BURNED, true);
        }
        if (argv[0].equals("state_cover_off"))
        {
            setState(self, STATE_COVER, false);
        }
        if (argv[0].equals("state_combat_off"))
        {
            setState(self, STATE_COMBAT, false);
        }
        if (argv[0].equals("state_peace_off"))
        {
            setState(self, STATE_PEACE, false);
        }
        if (argv[0].equals("state_aiming_off"))
        {
            setState(self, STATE_AIMING, false);
        }
        if (argv[0].equals("state_meditate_off"))
        {
            setState(self, POSTURE_SNEAKING, false);
        }
        if (argv[0].equals("state_berserk_off"))
        {
            setState(self, STATE_BERSERK, false);
        }
        if (argv[0].equals("state_feigndeath_off"))
        {
            setState(self, STATE_FEIGN_DEATH, false);
        }
        if (argv[0].equals("state_ca_evasive_off"))
        {
            setState(self, STATE_CA_EVASIVE, false);
        }
        if (argv[0].equals("state_ca_normal_off"))
        {
            setState(self, STATE_CA_NORMAL, false);
        }
        if (argv[0].equals("sitting_off"))
        {
            setState(self, STATE_CA_AGGRESSIVE, false);
        }
        if (argv[0].equals("state_tumbling_off"))
        {
            setState(self, STATE_TUMBLING, false);
        }
        if (argv[0].equals("state_rallied_off"))
        {
            setState(self, STATE_RALLIED, false);
        }
        if (argv[0].equals("state_stunned_off"))
        {
            setState(self, STATE_STUNNED, false);
        }
        if (argv[0].equals("state_blinded_off"))
        {
            setState(self, STATE_BLINDED, false);
        }
        if (argv[0].equals("state_dizzy_off"))
        {
            setState(self, STATE_DIZZY, false);
        }
        if (argv[0].equals("state_intimidated_off"))
        {
            setState(self, STATE_INTIMIDATED, false);
        }
        if (argv[0].equals("state_immobilized_off"))
        {
            setState(self, STATE_IMMOBILIZED, false);
        }
        if (argv[0].equals("state_frozen_off"))
        {
            setState(self, STATE_FROZEN, false);
        }
        if (argv[0].equals("state_swimming_off"))
        {
            setState(self, STATE_SWIMMING, false);
        }
        if (argv[0].equals("state_sittingonchair_off"))
        {
            setState(self, STATE_SITTING_ON_CHAIR, false);
        }
        if (argv[0].equals("state_crafting_off"))
        {
            setState(self, STATE_CRAFTING, false);
        }
        if (argv[0].equals("state_glowing_jedi_off"))
        {
            setState(self, STATE_GLOWING_JEDI, false);
        }
        if (argv[0].equals("state_mask_scent_off"))
        {
            setState(self, STATE_MASK_SCENT, false);
        }
        if (argv[0].equals("state_poisoned_off"))
        {
            setState(self, STATE_POISONED, false);
        }
        if (argv[0].equals("state_bleeding_off"))
        {
            setState(self, STATE_BLEEDING, false);
        }
        if (argv[0].equals("state_diseased_off"))
        {
            setState(self, STATE_DISEASED, false);
        }
        if (argv[0].equals("state_on_fire_off"))
        {
            setState(self, STATE_ON_FIRE, false);
        }
        if (argv[0].equals("state_riding_mount_off"))
        {
            setState(self, STATE_RIDING_MOUNT, false);
        }
        if (argv[0].equals("state_mounted_off"))
        {
            setState(self, STATE_MOUNTED_CREATURE, false);
        }
        if (argv[0].equals("state_pship_off"))
        {
            setState(self, STATE_PILOTING_SHIP, false);
        }
        if (argv[0].equals("state_shipop_off"))
        {
            setState(self, STATE_SHIP_OPERATIONS, false);
        }
        if (argv[0].equals("state_sgunner_off"))
        {
            setState(self, STATE_SHIP_GUNNER, false);
        }
        if (argv[0].equals("state_ship_inside_off"))
        {
            setState(self, STATE_SHIP_INTERIOR, false);
        }
        if (argv[0].equals("state_pilot_pob_off"))
        {
            setState(self, STATE_PILOTING_POB_SHIP, false);
        }
        if (argv[0].equals("state_performdb_off"))
        {
            setState(self, STATE_PERFORM_DEATHBLOW, false);
        }
        if (argv[0].equals("state_disguise_off"))
        {
            setState(self, STATE_DISGUISE, false);
        }
        if (argv[0].equals("state_electric_burned_off"))
        {
            setState(self, STATE_ELECTRIC_BURNED, false);
        }
        if (argv[0].equals("state_cold_burned_off"))
        {
            setState(self, STATE_COLD_BURNED, false);
        }
        if (argv[0].equals("state_acid_burned_off"))
        {
            setState(self, STATE_ACID_BURNED, false);
        }
        if (argv[0].equals("state_energy_burned_off"))
        {
            setState(self, STATE_ENERGY_BURNED, false);
        }
        if (argv[0].equals("state_kinetic_burned_off"))
        {
            setState(self, STATE_KINETIC_BURNED, false);
        }
        return SCRIPT_CONTINUE;
    }
}