package script.terminal;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;
import script.library.ai_lib;
import script.library.sui;
import script.library.colors;
import script.library.prose;
import script.library.static_item;
import script.library.trial;
import script.library.buff;
import script.library.anims;

public class aaa_test_terminal extends script.base_script
{
    public aaa_test_terminal()
    {
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
    {
		menu_info_data mid = mi.getMenuItemByType(menu_info_types.SERVER_MENU1);
		int menu = mi.addRootMenu(menu_info_types.SERVER_MENU1, new string_id("smuggler_black_market_v1", "open_black_market_terminal"));
		if(isGod(player))
		{
		mi.addSubMenu(menu, menu_info_types.SERVER_MENU2, new string_id("smuggler_black_market_v1", "debug_info"));
		setObjvar(self, "black_market.used", 1);
		}
		return SCRIPT_CONTINUE;
    }
	public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
		if (item == menu_info_types.SERVER_MENU1)
		{
			openBlackMarketTerminal(player);
			sendSystemMessageTestingOnly(player, "TBD TBD TBD TBD TBD");
        }
		if (item == menu_info_types.SERVER_MENU2)
		{
			sui.msgbox
			sendSystemMessageTestingOnly(player, "TBD TBD TBD TBD TBD");
        }
        
    }
}