package script.terminal;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;
import script.library.ai_lib;
import script.library.sui;
import script.library.colors;
import script.library.prose;
import script.library.trial;
import script.library.buff;
import script.library.anims;

public class aaa_test_terminal extends script.base_script
{
    public aaa_test_terminal()
    {
    }
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
    {
		menu_info_data mid = mi.getMenuItemByType(menu_info_types.SERVER_MENU1);
		int menu = mi.addRootMenu(menu_info_types.SERVER_MENU1, new string_id("smuggler_black_market_v1", "open_black_market_terminal"));
		if(isGod(player))
		{
		mi.addSubMenu(menu, menu_info_types.SERVER_MENU2, new string_id("smuggler_black_market_v1", "debug_info"));
		setObjvar(self, "black_market.gm", 1);
		}
		return SCRIPT_CONTINUE;
    }
	
	public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
		if (item == menu_info_types.SERVER_MENU1)
		{
			openBlackMarketTerminal(player);
			sendSystemMessageTestingOnly(player, "TBD TBD TBD TBD TBD");
        }
		if (item == menu_info_types.SERVER_MENU2)
		{
	
			sendSystemMessageTestingOnly(player, "TBD TBD TBD TBD TBD");
        }
        
    }
	public int openBlackMarketTerminal(obj_id self, dictionary params) throws InterruptedException
    {
        String BMData = "Items for Sale:";
        String uiTitle = "Smuggler Black Market";
        int page = createSUIPage("/Script.barMenu", self, self);
        setSUIProperty(page, "comp.text", "Text", BMData);
		setSUIProperty(page, "comp.b.1.drink", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.b.1.blank", "Image", "loading/generic/generic_wookiearmor");
		setSUIProperty(page, "comp.price", "Text", "Select Item #");
		setSUIProperty(page, "comp.b.tablepage.containerall.payment.New Data", "Data", "1,500");
        setSUIProperty(page, "bg.caption.lblTitle", "Text", uiTitle);
        setSUIProperty(page, "Prompt.lblPrompt", "Editable", "false");
        setSUIProperty(page, "Prompt.lblPrompt", "GetsInput", "false");
        setSUIProperty(page, "btnCancel", "Visible", "false");
        setSUIProperty(page, "btnRevert", "Visible", "false");
        setSUIProperty(page, "btnOk", sui.PROP_TEXT, "Purchase");
        subscribeToSUIEvent(page, sui_event_type.SET_onClosedOk, "%button0%", "handleBarReward");
        showSUIPage(page);
        flushSUIPage(page);
        return SCRIPT_CONTINUE;
    }
	public int handleBarReward(obj_id self, dictionary params) throws InterruptedException
	{
		if (params == null || params.isEmpty())
        {
            return SCRIPT_CONTINUE;
        }
        obj_id player = sui.getPlayerId(params);
        int bp = sui.getIntButtonPressed(params);
        if (bp == sui.BP_CANCEL)
        {
            return SCRIPT_CONTINUE;
        }
        String message = sui.getInputBoxText(params);
        if (message == null)
        {
            sendSystemMessage(self, new string_id(STF, "not_valid_message"));
            return SCRIPT_CONTINUE;
        }
        setName(self, message);
        return SCRIPT_CONTINUE;
		sendSystemMessageTestingOnly(self, "Item Purchased.");
		return SCRIPT_OVERRIDE;
	}
}