/*--------------------------------------------------------------------------------------------------------------------------------------
//Plotting System
- Features:
Easily make cities or spawn buildings with plots or use to spawn objects (needs refactoring)
________________________________________________________________________________________________________________________________________

- Todo:
DONE: Add in yaw rotation
DONE :spawning the building
NOT DONE: City support
NOT DONE/HOW DO I EVEN DO THIS: Signs
NOT DONE: Player ownership
//

Ideas:
Use this for saving houses. if a player has the housing deed in the inventory drag onto the plot and it will build. not sure if the orientation will save since its 2 scripts BUT YA KNOW WHAT..
----------------------------------------------------------------------------------------------------------------------------------------
*/
package script.marker;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.ai_lib;
import script.library.armor;
import script.library.beast_lib;
import script.library.buff;
import script.library.callable;
import script.library.chat;
import script.library.consumable;
import script.library.craftinglib;
import script.library.create;
import script.library.expertise;
import script.library.factions;
import script.library.gm;
import script.library.groundquests;
import script.library.healing;
import script.library.incubator;
import script.library.instance;
import script.library.jedi;
import script.library.money;
import script.library.pet_lib;
import script.library.player_stomach;
import script.library.prose;
import script.library.resource;
import script.library.respec;
import script.library.skill;
import script.library.skill_template;
import script.library.space_crafting;
import script.library.space_flags;
import script.library.space_skill;
import script.library.space_transition;
import script.library.space_utils;
import script.library.static_item;
import script.library.stealth;
import script.library.sui;
import script.library.utils;
import script.library.weapons;
import script.library.performance;

public class housing extends script.base_script
{
    public housing()
    {
    }
    public static final String STF = "city/city";
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
        {
            menu_info_data mid = mi.getMenuItemByType(menu_info_types.ITEM_USE);
            int menu = mi.addRootMenu(menu_info_types.ITEM_USE, new string_id("housing_plot", "generate_structure")); // TODO: Check for resources to build house.
            int menu2 = mi.addRootMenu(menu_info_types.SERVER_MENU1, new string_id("housing_plot","read_about"));//Not sure if this is really needed
            if(isGod(player))
            {
                int menu1 = mi.addRootMenu(menu_info_types.SERVER_MENU2, new string_id("housing_plot", "setup")); //Debug for god to change housingTemplate. TODO: add listbox to change it.
                mi.addSubMenu(menu1, menu_info_types.SERVER_MENU3, new string_id("housing_plot", "confirm"));
            }
            return SCRIPT_CONTINUE;
        }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
        if (item == menu_info_types.ITEM_USE) // Click plot terminal/thing
        {
            String house = getStringObjVar(self, "plot.template");
            if (house == null)
            {
                sendSystemMessageTestingOnly(player, "[Error]: Missing housing data.");
                return SCRIPT_CONTINUE;
            }
            location plotTerminal = getLocation(self);
            obj_id structure = createObject(house, plotTerminal);
            setYaw(structure, getYaw(self)); // House should face terminal's orientation. although it will solely depend on the model.
            debugConsoleMsg(player, "Structure Generated.");
            destroyObject(self);
    }
    if (item == menu_info_types.SERVER_MENU1)//Second Menu, Information request
        {
            sendSystemMessageTestingOnly(player, "Information Request Recieved");
            //TODO: add information box. (int didnt wanna work sooo)
        }
        if (item == menu_info_types.SERVER_MENU2)//God menu (for now until I can figure out a practical use for this.)
        {
            String housingTemplate = "";
            String buffer = "Enter House or Furniture template.";
            String title = "Housing Plot Menu";
            sui.filteredInputbox(self, player, buffer, title, "handleTemplate", housingTemplate);
        }
        if (item == menu_info_types.SERVER_MENU3)//confirmation. WIP
        {
            //String message2 = "";
            //String buffer = "Are you sure you want to :";
            //String title = "Plot Confirmation Window";
            sendSystemMessageTestingOnly(player, "WIP: Add confirmation and then lock the template to current one.");
        }
        return SCRIPT_CONTINUE;
    }
    public int handleTemplate(obj_id self, dictionary params) throws InterruptedException
    {
        if (params == null || params.isEmpty())
        {
            return SCRIPT_CONTINUE;
        }
        obj_id player = sui.getPlayerId(params);
        int bp = sui.getIntButtonPressed(params);
        if (bp == sui.BP_CANCEL)
        {
            return SCRIPT_CONTINUE;
        }
        String housingTemplate = sui.getInputBoxText(params);
        if (housingTemplate == null)
        {
            sendSystemMessage(self, new string_id(STF, "not_valid_message"));
            return SCRIPT_CONTINUE;
        }
        if (!housingTemplate.startsWith("object/building/"))
        {
            sendSystemMessageTestingOnly(player, "Invalid housing template.");
            return SCRIPT_CONTINUE;
        }
        if (!housingTemplate.endsWith(".iff"))
        {
            sendSystemMessageTestingOnly(player, "Invalid housing template.");
            return SCRIPT_CONTINUE;
        }
        if (housingTemplate.startsWith("object/building/player"))
        {
            sendSystemMessageTestingOnly(player, "Player House detected.");
            setObjVar(self, "plot.template", housingTemplate);
            setObjVar(self, "plot.player_house", housingTemplate);
            return SCRIPT_CONTINUE;
        }
        if (!housingTemplate.startsWith("object/building"))
        {
            sendSystemMessageTestingOnly(player, "Non-building object set as marker. NOTE: this can cause issues if not setup right!");
            return SCRIPT_CONTINUE;
        }
        setObjVar(self, "plot.template", housingTemplate);
        return SCRIPT_CONTINUE;
    }
}