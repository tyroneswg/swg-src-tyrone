//TODO: make it an inventory only tool to avoid salty usage, make it 1 time use and disable custom colors.
package script.rename;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.ai_lib;
import script.library.armor;
import script.library.beast_lib;
import script.library.buff;
import script.library.callable;
import script.library.chat;
import script.library.consumable;
import script.library.craftinglib;
import script.library.create;
import script.library.expertise;
import script.library.factions;
import script.library.gm;
import script.library.groundquests;
import script.library.healing;
import script.library.incubator;
import script.library.instance;
import script.library.jedi;
import script.library.money;
import script.library.pet_lib;
import script.library.player_stomach;
import script.library.prose;
import script.library.resource;
import script.library.respec;
import script.library.skill;
import script.library.skill_template;
import script.library.space_crafting;
import script.library.space_flags;
import script.library.space_skill;
import script.library.space_transition;
import script.library.space_utils;
import script.library.static_item;
import script.library.stealth;
import script.library.sui;
import script.library.utils;
import script.library.weapons;
import script.library.performance;

public class tool extends script.base_script
{
    public tool()
    {
    }
	public static final String STF = "city/city";
	public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
		{
			menu_info_data mid = mi.getMenuItemByType(menu_info_types.ITEM_USE);
			int menu = mi.addRootMenu(menu_info_types.ITEM_USE, new string_id("renamer_tool", "rename_with_filter"));//stf filter? is that even a thing
			return SCRIPT_CONTINUE;
		}
	public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
	{
		if (item == menu_info_types.ITEM_USE)
		{
            String newName = "";
			String buffer = "Enter a new name for the targeted item.";
			String title = "Item Renamer Tool";
			sui.filteredInputbox(self, player, buffer, title, "handleMSGRequest", newName);
		}
        return SCRIPT_CONTINUE;
    }
	public int handleMSGRequest(obj_id self, dictionary params) throws InterruptedException
    {
        if (params == null || params.isEmpty())
        {
            return SCRIPT_CONTINUE;
        }
        obj_id player = sui.getPlayerId(params);
        obj_id itemTarget = getIntendedTarget(player);
        int bp = sui.getIntButtonPressed(params);
        if (bp == sui.BP_CANCEL)
        {
            return SCRIPT_CONTINUE;
        }
        String newName = sui.getInputBoxText(params);
        if (newName == null)
        {
            sendSystemMessage(self, new string_id(STF, "not_valid_message"));
            return SCRIPT_CONTINUE;
        }
        if (newName != null && (!isIdValid(player)))
        {
            sendSystemMessageTestingOnly(player, "You cannot rename a player you salty nerd.");
            return SCRIPT_CONTINUE;
        }
        setName(itemTarget, newName);// if the target isnt a player, pet or familiar. Maybe making the item no trade after naming it to avoid scamming?
        //destroyObject(self);
        return SCRIPT_CONTINUE;
    }
}