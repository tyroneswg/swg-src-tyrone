package script.conversation;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.library.utils;
import script library.npe;
import script library.badge;

public class level_90_btt extends script.base_script
{
    public level_90_btt()
    {
    }
	public static final string_id SID_TSS = new string_id("custom", "tansarii");
    public int OnAttach(obj_id self) throws InterruptedException
    {
        setName(self, "Calmar Iee (an explorer)");
        return SCRIPT_CONTINUE;
    }
	public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
        if (!badge.hasBadge(player, "npe_finish_all_pilot"))
		npe.movePlayerFromFalconToSharedStation(player);
        return SCRIPT_CONTINUE;
    }