for file in $(find ./dsrc/sku.0/sys.server/compiled/game/datatables/loot/loot_types/tcg -name "*.tab" | cut -sd / -f 2-)
do
	# Build output path (replace dsrc with data and tab to iff)
	outputfile="${file/.tab/.iff}"
	outputfile="${outputfile/dsrc/data}"
	
	# Make sure directory exists
	dir=$(dirname "${outputfile}")
	mkdir -p $dir 

	# build datatable file
	compilers/DataTableTool -i $file -o $outputfile
done 