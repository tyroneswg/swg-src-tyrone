#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=127.0.0.1
DBUSERNAME=swg
DBPASSWORD=swg
HOSTIP=127.0.0.1
CLUSTERNAME=swg
NODEID=
DSRC_DIR=
DATA_DIR=


read -p "Import database? (y/n) " response
#response=${response,,}
#if [[ $response =~ ^(yes|y| ) ]]; then
	cd $basedir/src/server/src/game/server/database/build/linux

	if [[ -z "$DBSERVICE" ]]; then
		echo "Enter the DSN for the database connection "
		read DBSERVICE
	fi

	if [[ -z "$DBUSERNAME" ]]; then
		echo "Enter the database username "
		read DBUSERNAME
	fi
	

	if [[ -z "$DBPASSWORD" ]]; then
		echo "Enter the database password "
		read DBPASSWORD
	fi

	perl ./database_update.pl --username=$DBUSERNAME --password=$DBPASSWORD --service=$DBSERVICE --goldusername=$DBUSERNAME --loginusername=$DBUSERNAME --createnewcluster --packages

	echo "Loading template list"
	perl ../../templates/processTemplateList.pl < ../../../../../../../../dsrc/sku.0/sys.server/built/game/misc/object_template_crc_string_table.tab > templates.sql
	sqlplus ${DBUSERNAME}/${DBPASSWORD}@${DBSERVICE} @templates.sql > templates.out
	cd $basedir
#fi

echo "Build complete!"